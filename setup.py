from setuptools import setup


setup(name='cse-scan',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='CSEntry scanner',
      url='https://gitlab.esss.lu.se/ics-infrastructure/csentry-scanner',
      author='Benjamin Bertrand',
      author_email='benjamin.bertrand@esss.se',
      license='BSD',
      py_modules=['cse_scan'],
      install_requires=[
          'Pillow',
          'PyYAML',
          'pyserial',
          'requests',
          'csentry-api',
      ],
      entry_points='''
          [console_scripts]
          cse-scan=cse_scan:main
      '''
      )
